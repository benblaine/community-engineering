# community-engineering

Tools and hacks to help you engineer your community :) You can read a bit more about what I think about [community engineering here](https://levelup.gitconnected.com/what-is-a-community-engineer-c175ce2e0a98).